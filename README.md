# Quality Metrics for Explainable AI Systems
## by Taiane Linhares

Bachelor's Thesis for the course "Digital Media & Technology" at TU-Berlin



## Content

### Reproducibility Jupyter Notebook
- Generates csv file for Amazon Mechanical Turk (AMT) crowdsourcing experiment;
- Plots visualizations with the modified SHAP package;
- Loads the anonymized AMT experiment results data set and format it to data analysis;

### SHAP2 package
- Extends bar and text plots to display hierarchical values, most relevant features, and maximal number of features;
- Obs.: copy this folder in your python library directory.

### AMT experiment files
- Experiment html file to be uploaded on the AMT website;
- AMT batches CSV file used in the experiment;
- Images and gifs used on the experiment website;
- Plots in the training data set;


